import sounddevice as sd
from speech_to_text import SpeechToText


try:
    while True:
        input("Press <Enter> and then speak something for 5 seconds.")

        sample_rate = 16_000  # 16 kHz
        duration = 5
        myrecording = sd.rec(int(duration * sample_rate), samplerate=sample_rate, channels=1, dtype='float64')
        sd.wait()

        stt = SpeechToText()
        text = stt.transcribe(myrecording.reshape(-1), sample_rate)
        print("You said: ", text)

except KeyboardInterrupt:
    pass
