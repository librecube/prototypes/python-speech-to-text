from transformers import SpeechT5Processor, SpeechT5ForSpeechToText


processor = SpeechT5Processor.from_pretrained("microsoft/speecht5_asr")
model = SpeechT5ForSpeechToText.from_pretrained("microsoft/speecht5_asr")


class SpeechToText:

    def __init__(self):
        pass

    def transcribe(self, recording, sample_rate):
        inputs = processor(audio=recording, sampling_rate=sample_rate, return_tensors="pt")
        predicted_ids = model.generate(**inputs, max_length=100)
        text = processor.batch_decode(predicted_ids, skip_special_tokens=True)
        return text
